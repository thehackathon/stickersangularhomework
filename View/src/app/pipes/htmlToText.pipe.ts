import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'htmlToText'
})
export class HtmlToTextPipe implements PipeTransform {
  public transform(html: string, len?: number): string {
    let result: string = html
      .replace('</p>', '\n')
      .replace(/<\/?[^>]+(>|$)/g, ' ')
      .replace(/&nbsp;/g, ' ')
      .trim();
    if (len && result.length > len + 3) {
      result = result.substring(0, len);
      result += '...';
    }
    return result;
  }
}
