import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'dynamicDate'
})
export class DynamicDatePipe extends DatePipe implements PipeTransform {
  private isToday(date: Date): boolean {
    const today = new Date();
    date = new Date(date);
    // date.setHours(0, 0, 0, 0);
    // today.setHours(0, 0, 0, 0);
    // return date.getTime() === today.getTime();
    return (
      date.getFullYear() === today.getFullYear() &&
      date.getMonth() === today.getMonth() &&
      date.getDate() === today.getDate()
    );
  }

  public transform(date: Date): string {
    return super.transform(date, this.isToday(date) ? 'HH:mm' : 'MMM d');
  }
}
