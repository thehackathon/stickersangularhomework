import {Component, OnInit} from '@angular/core';

import {Sticker} from '../../sticker-management/models/sticker.model';
import {StickerService} from '../../sticker-management/services/sticker.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public stickers: Sticker[] = [];

  public constructor(private service: StickerService) {
  }

  public ngOnInit(): void {
    this.service.load().subscribe(stickers => {
      this.stickers = stickers;
    });
  }

  protected delete(id: string): void {
    const stickerIdx: number = this.stickers.findIndex(s => s.Id === id);
    this.service.delete(id).subscribe(
      () => this.stickers.splice(stickerIdx, 1)
    );
  }
}
