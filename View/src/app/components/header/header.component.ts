import {Router, NavigationEnd} from '@angular/router';
import {Component, OnInit} from '@angular/core';

import {StickerService} from '../../sticker-management/services/sticker.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private static timeUpdateInterval: number = 2 * 1000;
  // 2 sec
  protected now: Date = new Date();
  protected isCreateStickerButtonShown: boolean;

  public constructor(private router: Router, private service: StickerService) {
  }

  public ngOnInit(): void {
    setInterval(() => {
      this.now = new Date();
    }, HeaderComponent.timeUpdateInterval);
    // header time update

    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.isCreateStickerButtonShown = e.url === '/';
      }
    });
    // show or hide button
  }

  protected createSticker(): void {
    this.service.createEmpty().subscribe(sticker => {
      this.router.navigate(
        ['/sticker-edit', sticker.Id],
      );
    });
  }
}
