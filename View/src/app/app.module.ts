import { StickerModule } from './sticker-management/sticker.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './components/app/app.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StickerModule,
    HttpClientModule
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }

//  TODO:
//  filter by name
//  modal window for remove sticker
//  modal window for leave sticker editor without save
//  reverse sticker-list
//  @media for phones
//  button 'scroll up' for dashboard
//  delete buttons
//  popover with info
//  preloader
//
//  mb refactor app structure
//  mb add tooltips
//  mb make header fixed
