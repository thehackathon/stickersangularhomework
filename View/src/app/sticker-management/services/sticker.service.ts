import { Observable } from 'rxjs';
import { Sticker } from '../models/sticker.model';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export abstract class StickerService {
  abstract get(id: string): Observable<Sticker>;
  abstract load(): Observable<Sticker[]>;
  abstract create(sticker: Sticker): Observable<Sticker>;
  abstract createEmpty(): Observable<Sticker>;
  abstract update(sticker: Sticker): Observable<void>;
  abstract delete(id: string): Observable<void>;
}
