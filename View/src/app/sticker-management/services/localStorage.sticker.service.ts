import {map} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';

import {Sticker} from '../models/sticker.model';
import {StickerService} from './sticker.service';
import {STICKERS, EMPTY} from '../components/sticker.mock';

@Injectable()
export class LocalStorageStickerService implements StickerService {
  private key = 0;
  private collectionName = 'StickerList';

  public constructor() {
    const val: Sticker[] = JSON.parse(localStorage.getItem(this.collectionName));
    this.key = val ? Math.max(...val.map(s => +s.Id)) + 1 : this.init();
  }

  private init(): number {
    this.save(STICKERS);
    return STICKERS.length;
  }

  private save(stickers: Sticker[]): void {
    localStorage.setItem(this.collectionName, JSON.stringify(stickers));
  }

  public get(id: string): Observable<Sticker> {
    return this.load().pipe(
      map((stickers: Sticker[]) =>
        stickers.find(sticker => sticker.Id === id))
    );
  }

  public load(): Observable<Sticker[]> {
    return of(JSON.parse(localStorage.getItem(this.collectionName)));
  }

  public create(sticker: Sticker): Observable<Sticker> {
    sticker.Id = `${this.key++}`;
    return this.load().pipe(map(stickers => {
      stickers.push(sticker);
      this.save(stickers);
      return sticker;
    }));
  }

  public createEmpty(): Observable<Sticker> {
    return this.create(EMPTY);
  }

  public update(sticker: Sticker): Observable<void> {
    const collection: Observable<Sticker[]> = this.load();
    return collection.pipe(
      map(stickers => {
        const idx: number = stickers.findIndex(s => s.Id === sticker.Id);
        sticker.LastModificationDate = new Date();
        stickers[idx] = sticker;
        this.save(stickers);
        return;
      }));
  }

  public delete(id: string): Observable<void> {
    const collection: Observable<Sticker[]> = this.load();
    return collection.pipe(map(stickers => {
      const idx: number = stickers.findIndex(item => item.Id === id);
      stickers.splice(idx, 1);
      this.save(stickers);
      return;
    }));
  }
}
