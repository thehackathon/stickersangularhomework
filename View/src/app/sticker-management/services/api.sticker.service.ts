import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Sticker} from '../models/sticker.model';
import {StickerService} from './sticker.service';

interface CreateResponse {
  name: string;
}

@Injectable()
export class ApiStickerService implements StickerService {
  static prefix = 'angular-stickers';
  static url = `https://${ApiStickerService.prefix}.firebaseio.com/`;

  constructor(private http: HttpClient) {
  }

  get(id: string): Observable<Sticker> {
    return this.http
      .get<Sticker>(`${ApiStickerService.url}/stickers/${id}.json`)
      .pipe(map(sticker => {
        if (!sticker) {
          return null;
        }
        sticker.Id = id;
        return sticker;
      }));
  }

  load(): Observable<Sticker[]> {
    return this.http
      .get<Sticker[]>(`${ApiStickerService.url}/stickers.json`)
      .pipe(map(stickers => {
        if (!stickers) {
          return [];
        }
        return Object.keys(stickers).map(key => ({...stickers[key], Id: key}));
      }));
  }

  create(sticker: Sticker): Observable<Sticker> {
    return this.http
      .post<CreateResponse>(`${ApiStickerService.url}/stickers.json`, sticker)
      .pipe(map(res => {
        return {...sticker, Id: res.name};
      }));
  }

  createEmpty(): Observable<Sticker> {
    const sticker: Sticker = {
      Id: null,
      Name: '<no name>',
      CreationDate: new Date(),
      LastModificationDate: new Date(),
      HTML: ''
    };
    return this.create(sticker);
  }

  update(sticker: Sticker): Observable<void> {
    return this.http
      .put<void>(`${ApiStickerService.url}/stickers/${sticker.Id}.json`, sticker);
  }

  delete(id: string): Observable<void> {
    return this.http
      .delete<void>(`${ApiStickerService.url}/stickers/${id}.json`);
  }
}
