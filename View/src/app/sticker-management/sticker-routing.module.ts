import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StickerViewComponent } from './components/sticker-view/sticker-view.component';
import { StickerEditComponent } from './components/sticker-edit/sticker-edit.component';
import { StickerComponent } from './components/sticker/sticker.component';

const routes: Routes = [
  {
    path: 'sticker-view/:Id',
    component: StickerViewComponent
  },
  {
    path: 'sticker-edit/:Id',
    component: StickerEditComponent,
  },
  {
    path: 'sticker',
    component: StickerComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class StickerRoutingModule { }
