import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {Sticker} from '../../models/sticker.model';

@Component({
  selector: 'app-sticker',
  templateUrl: './sticker.component.html',
  styleUrls: [
    './sticker.component.scss',
  ],
})
export class StickerComponent implements OnInit {
  @Input() sticker: Sticker;
  @Output() deleteSticker = new EventEmitter<string>();

  public ngOnInit(): void {
  }

  protected _deleteSticker(id: string): void {
    this.deleteSticker.emit(id);
  }
}
