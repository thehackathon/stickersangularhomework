import {Sticker} from '../models/sticker.model';

export const EMPTY: Sticker = {
  Id: null,
  Name: '<no name>',
  CreationDate: new Date(),
  LastModificationDate: new Date(),
  HTML: ''
};

export const STICKERS: Sticker[] = [
  {
    Id: '0',
    Name: 'docker knowledge base',
    CreationDate: new Date('2019/11/08'),
    LastModificationDate: new Date('2019/11/08'),
    HTML: `<p><strong><u>Uninstall old versions</u></strong></p><p style=\"margin-left: 20px;\">
    $<em> sudo apt-get remove docker docker-engine docker.io containerd runc</em></p><p><strong>
    <u>Set up the repository</u></strong></p><p style=\"margin-left: 20px;\">$<em> sudo apt-get update</em></p>
    <p style=\"margin-left: 20px;\"><br></p><p style=\"margin-left: 20px;\">$<em> sudo apt-get install \\
    </em></p><p style=\"margin-left: 20px;\"><em>&nbsp; &nbsp; apt-transport-https \\</em></p><p style=\"margin-left: 20px;\">
    <em>&nbsp; &nbsp; ca-certificates \\</em></p><p style=\"margin-left: 20px;\"><em>&nbsp; &nbsp; curl \\</em>
    </p><p style=\"margin-left: 20px;\"><em>&nbsp; &nbsp; gnupg-agent \\</em></p><p style=\"margin-left: 20px;\">
    <em>&nbsp; &nbsp; software-properties-common</em></p><p style=\"margin-left: 20px;\"><br></p><p style=\"margin-left: 20px;\">
    $<em> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -</em></p><p style=\"margin-left: 20px;\">
    <br></p><p style=\"margin-left: 20px;\">$<em> sudo add-apt-repository \\</em></p><p style=\"margin-left: 20px;\">
    <em>&nbsp; &nbsp;\"deb [arch=amd64] https://download.docker.com/linux/ubuntu \\</em></p><p style=\"margin-left: 20px;\">
    <em>&nbsp; &nbsp;$(lsb_release -cs) \\</em></p><p style=\"margin-left: 20px;\"><em>&nbsp; &nbsp;stable\"</em>
    </p><p style=\"margin-left: 20px;\"><br></p><p><strong><u>Install docker engine</u></strong></p><p style=\"margin-left: 20px;\">
    $<em> sudo apt-get update</em></p><p style=\"margin-left: 20px;\">$<em> sudo apt-get install docker-ce docker-ce-cli containerd.io</em>
    </p><p><br></p><p><strong><u>If you would like to use Docker as a non-root user, you should now consider adding your user to the“
    docker” group with something like:</u></strong></p><p style=\"margin-left: 20px;\">$ <em>sudo usermod -aG docker your-user</em></p>`
  },
  {
    Id: '1',
    Name: 'Sticker name 02',
    CreationDate: new Date(),
    LastModificationDate: new Date(),
    HTML: '<strong>Sticker 2</strong>'
  },
  {
    Id: '2',
    Name: 'Sticker name 03',
    CreationDate: new Date('2019/11/08'),
    LastModificationDate: new Date('2019/11/08'),
    HTML: '<strong>I am 3</strong>'
  },
  {
    Id: '3',
    Name: 'Sticker name 04',
    CreationDate: new Date('2019/11/08'),
    LastModificationDate: new Date(),
    HTML: '<strong>Hello 04</strong>'
  },
  {
    Id: '4',
    Name: 'Sticker name 05',
    CreationDate: new Date('2019/11/08'),
    LastModificationDate: new Date('2019/11/08'),
    HTML: '<strong>Hello 05</strong>'
  }
];
