import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {Sticker} from '../../models/sticker.model';
import {StickerService} from '../../services/sticker.service';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-sticker-edit',
  templateUrl: './sticker-edit.component.html',
  styleUrls: [
    './sticker-edit.component.scss',
  ]
})
export class StickerEditComponent implements OnInit {
  protected sticker$: Observable<Sticker>;

  constructor(private router: Router, private route: ActivatedRoute, private service: StickerService) {
  }

  public ngOnInit(): void {
    this.sticker$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.get(params.get('Id')))
    );
  }

  protected save(sticker: Sticker): void {
    this.service.update(sticker).subscribe(
      () => this.router.navigateByUrl('/')
    );
  }
}
