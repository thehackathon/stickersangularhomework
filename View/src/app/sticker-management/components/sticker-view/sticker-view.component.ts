import {Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';

import {Sticker} from '../../models/sticker.model';
import {StickerService} from '../../services/sticker.service';

@Component({
  selector: 'app-sticker-view',
  templateUrl: './sticker-view.component.html',
  styleUrls: ['./sticker-view.component.scss']
})
export class StickerViewComponent implements OnInit {
  protected sticker$: Observable<Sticker>;

  public constructor(private route: ActivatedRoute, private service: StickerService) {
  }

  public ngOnInit(): void {
    this.sticker$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.get(params.get('Id')))
    );
  }
}
