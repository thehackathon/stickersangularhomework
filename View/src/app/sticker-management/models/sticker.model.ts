export interface Sticker {
  Id: string;
  Name: string;
  HTML: string;
  CreationDate: Date;
  LastModificationDate: Date;
}
