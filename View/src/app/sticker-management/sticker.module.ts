import {HtmlToTextPipe} from './../pipes/htmlToText.pipe';
import {DynamicDatePipe} from './../pipes/dynamic-date.pipe';

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import {StickerRoutingModule} from './sticker-routing.module';

import {StickerComponent} from './components/sticker/sticker.component';
import {StickerEditComponent} from './components/sticker-edit/sticker-edit.component';
import {StickerViewComponent} from './components/sticker-view/sticker-view.component';

import {StickerService} from './services/sticker.service';
import {ApiStickerService} from './services/api.sticker.service';
import {LocalStorageStickerService} from './services/localStorage.sticker.service';


@NgModule({
  declarations: [
    StickerComponent,
    StickerEditComponent,
    StickerViewComponent,
    DynamicDatePipe,
    HtmlToTextPipe,
  ],
  imports: [
    StickerRoutingModule,
    CommonModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    FormsModule,
  ],
  exports: [
    StickerComponent,
  ],
  providers: [
    {provide: StickerService, useClass: ApiStickerService},
  ],
})
export class StickerModule {
}
